## [1.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-epub-js/compare/v1.1.2...v1.1.3) (2018-08-12)


### Bug Fixes

* renaming package to clarify EPUB 2 output ([0012cf1](https://gitlab.com/mfgames-writing/mfgames-writing-epub-js/commit/0012cf1))

## [1.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-epub-js/compare/v1.1.1...v1.1.2) (2018-08-11)


### Bug Fixes

* adding package mangagement ([a6cef52](https://gitlab.com/mfgames-writing/mfgames-writing-epub-js/commit/a6cef52))
