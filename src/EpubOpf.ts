import matter = require("gray-matter");
import { EditionArgs, MetadataDescriptionData } from "@mfgames-writing/contracts";
import * as mo from "@mfgames-writing/opf";
import * as fs from "fs";
import * as marked from "marked";
import * as moment from "moment";

export function addOpfMetadata(
    args: EditionArgs,
    opf: mo.Opf,
)
{
    // Add in the single identifier.
    opf.identifiers.push(new mo.OpfIdentifier(args.edition.uid));
    opf.title = args.edition.title;

    // We always have to have a language.
    opf.language = args.edition.language || "en";

    // Add in specific metadata fields.
    addAuthors(args, opf);
    addDates(args, opf);
    addDescription(args, opf);
    addEditors(args, opf);
    addPublishers(args, opf);
    addRights(args, opf);
    addSeries(args, opf);
    addSubjects(args, opf);

    // We always have an NCX file.
    opf.manifests
        .push(new mo.OpfManifest("ncx", "toc.ncx", "application/x-dtbncx+xml"));
    opf.toc = "ncx";
    opf.coverImage = "cover-image";
}

function addAuthors(
    args: EditionArgs,
    opf: mo.Opf
)
{
    if (args.edition.author)
    {
        if (typeof args.edition.author === "string")
        {
            args.edition.author = [args.edition.author];
        }

        for (const author of args.edition.author)
        {
            opf.creators.push(new mo.OpfCreator(author, "aut"));
        }
    }
}

function addDates(
    args: EditionArgs,
    opf: mo.Opf
)
{
    if (args.edition.date)
    {
        opf.dates
            .push(new mo.OpfDate(moment(args.edition.date)));
    }
}

function addDescription(
    args: EditionArgs,
    opf: mo.Opf
)
{
    // If we don't have a description, then we don't need to do anything.
    if (!args.edition.description)
    {
        return;
    }

    // Normalize the description field.
    if (typeof args.edition.description === "string")
    {
        args.edition.description = {
            contents: args.edition.description,
        } as MetadataDescriptionData;
    }

    // Figure out how we are going to add this.
    if (args.edition.description.contents)
    {
        // Just set the description directly.
        opf.description = args.edition.description.contents;
    }
    else if (args.edition.description.source)
    {
        // Load the file in as a Markdown without the YAML metadata.
        const buf = fs.readFileSync(args.edition.description.source);
        const mat = matter(buf);
        const cnt = mat.content;
        const htm = marked(cnt);

        opf.description = htm;
    }
}

function addEditors(
    args: EditionArgs,
    opf: mo.Opf
)
{
    if (args.edition.editor)
    {
        if (typeof args.edition.editor === "string")
        {
            args.edition.editor = [args.edition.editor];
        }

        for (const editor of args.edition.editor)
        {
            opf.contributors
                .push(new mo.OpfCreator(editor, "edt"));
        }
    }
}

function addPublishers(
    args: EditionArgs,
    opf: mo.Opf
)
{
    // Publishers are optional. Our system only allows for one but OPF allows
    // for many so we have to add it to the list.
    if (args.edition.publisher)
    {
        opf.publishers
            .push(new mo.OpfPublisher(args.edition.publisher));
    }
}

function addRights(
    args: EditionArgs,
    opf: mo.Opf,
): void
{
    if (args.edition.rights)
    {
        opf.rights = args.edition.rights;
    }
}

function addSeries(
    args: EditionArgs,
    opf: mo.Opf,
): void
{
    // If we don't have a series, then don't do anything.
    if (!args.edition.series)
    {
        return;
    }

    // Create the series container.
    opf.series = new mo.OpfSeries();

    // Add in the series elements.
    if (args.edition.series.name)
    {
        opf.series.name = args.edition.series.name;
    }

    if (args.edition.series.volume)
    {
        opf.series.volume = args.edition.series.volume;
    }
}

function addSubjects(
    args: EditionArgs,
    opf: mo.Opf
)
{
    // If we don't have subjects, just return it.
    if (!args.edition.subjects)
    {
        return;
    }

    // Add in the subjects.
    for (const subject of args.edition.subjects)
    {
        opf.subjects.push(subject);
    }
}
